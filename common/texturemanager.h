#ifndef TEXTUREMANAGER_H
#define TEXTUREMANAGER_H

#include <string>
#include <map>

class TextureManager
{
    TextureManager();
    virtual ~TextureManager();
    static TextureManager* _sInstance;
public:
    static TextureManager* CreateInstance();
    static TextureManager* Instance();
    static void DestroyInstance();

    unsigned int LoadTexture(const std::string& name, int& w, int& h);
    unsigned int LoadTextureFromFilename(const std::string& filename, int& w, int& h);

private:
    std::map<std::string, unsigned int> _textures;
};

#endif // TEXTUREMANAGER_H
