
#include <glad/glad.h>

#define SDL_MAIN_HANDLED
#include <SDL.h>

#include <iostream>
#include "application.h"
#include "../gameactions.h"

using namespace std;

System::System(int argc, char *argv[])
{
    for (int i = 0; i < argc; i++)
        this->_args.push_back(argv[i]);
}

System::~System()
{ }

double System::GetTime()
{
    return double(SDL_GetTicks()) / 1000.0;
}

class Window_SDL : public Application::Window
{
private:
    SDL_Window* _window;
    SDL_GLContext _context;

public:
    Window_SDL(const char* title, int width, int height)
        : Window(), _window(0), _context(0)
    {
        this->_window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
        if (this->_window != 0)
        {
            cout << "Window created" << endl;
            this->_context = SDL_GL_CreateContext(this->_window);
            if (this->_context == 0)
                cout << "Unable to create GL context..." << endl;
            else
                cout << "Context created" << endl;
        }
        else
            cout << "Unable to open Window..." << endl;
    }

    virtual ~Window_SDL()
    {
        SDL_GL_DeleteContext(this->_context);
        SDL_DestroyWindow(this->_window);
    }

    virtual void MakeCurrent()
    {
        SDL_GL_MakeCurrent(this->_window, this->_context);
    }

    virtual void Swap()
    {
        // Swap front and back rendering buffers
        SDL_GL_SwapWindow(this->_window);
    }

    bool IsValid()
    {
        return (this->_window != 0 && this->_context != 0);
    }

    SDL_Window* GetWindow() { return this->_window; }
};

Window_SDL* mainWindow = 0;

Application::Window* Application::MainWindow()
{
    return mainWindow;
}

Application::Window* Application::AddWindow(const char* title, int width, int height)
{
    return new Window_SDL(title, width, height);
}

void Application::CloseWindow(Application::Window* window)
{
    delete window;
}

int main(int argc, char* argv[])
{
    keyBinding.insert(std::make_pair(InputEvent(SDL_KEYDOWN, SDLK_SPACE), GameActions::NextGameMode));
    keyBinding.insert(std::make_pair(InputEvent(SDL_KEYDOWN, SDLK_HOME), GameActions::PrevGameMode));
    keyBinding.insert(std::make_pair(InputEvent(SDL_KEYDOWN, SDLK_LEFT), GameActions::Left));
    keyBinding.insert(std::make_pair(InputEvent(SDL_KEYDOWN, SDLK_RIGHT), GameActions::Right));
    keyBinding.insert(std::make_pair(InputEvent(SDL_KEYDOWN, SDLK_UP), GameActions::Up));
    keyBinding.insert(std::make_pair(InputEvent(SDL_KEYDOWN, SDLK_DOWN), GameActions::Down));
    keyBinding.insert(std::make_pair(InputEvent(SDL_KEYDOWN, SDLK_a), GameActions::Left));
    keyBinding.insert(std::make_pair(InputEvent(SDL_KEYDOWN, SDLK_d), GameActions::Right));
    keyBinding.insert(std::make_pair(InputEvent(SDL_KEYDOWN, SDLK_w), GameActions::Up));
    keyBinding.insert(std::make_pair(InputEvent(SDL_KEYDOWN, SDLK_s), GameActions::Down));
    keyBinding.insert(std::make_pair(InputEvent(SDL_KEYDOWN, SDLK_COMMA), GameActions::EnterOrLeaveCar));

    System sys(argc, argv);
    if (gApp != 0)
    {
        cout << "App Ok" << endl;
        if (gApp->InitializeApplication(&sys))
        {
            cout << "Application initialized" << endl;
            if (gApp->IsCommandLineApplication())
            {
                while (gApp->IsRunning())
                {
                    gApp->GameLoop();
                }
                gApp->Destroy();
            }
            else
            {
                bool running = true;
                if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) == 0)
                {
                    int major, minor;
                    bool core;
                    gApp->GetContextAttributes(major, minor, core);
                    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, major);
                    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minor);

                    int width = 800, height = 600;
                    mainWindow = new Window_SDL("OpenGL 3 test", width, height);
                    if (mainWindow->IsValid())
                    {
                        SDL_SetRelativeMouseMode(SDL_TRUE);

                        if(gladLoadGL())
                        {
                            cout << "Functions loaded" << endl;
                            if (gApp->InitializeGraphics())
                            {
                                gApp->Resize(width, height);

                                SDL_Event event;
                                cout << "Graphics initialized" << endl;
                                while (gApp->IsRunning() && running)
                                {
                                    while (SDL_PollEvent(&event))
                                    {
                                        if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) running = false;
                                        if (event.type == SDL_KEYDOWN || event.type == SDL_KEYUP)
                                        {
                                            auto e = InputEvent(SDL_KEYDOWN, event.key.keysym.sym);
                                            if (keyBinding.find(e) != keyBinding.end())
                                            {
                                                gApp->InputDigitalAction(keyBinding[e], event.type == SDL_KEYDOWN ? 1 : 0);
                                            }
                                        }
//                                        if (event.type == SDL_MOUSEMOTION)
//                                        {
//                                            gApp->MouseMove(event.motion.x - (width/2), event.motion.y - (height/2));
//                                        }
//                                        else if (event.type == SDL_KEYDOWN)
//                                        {
//                                            gApp->KeyAction(event.key.keysym.sym, event.key.state);
//                                        }
//                                        else if (event.type == SDL_KEYUP)
//                                        {
//                                            gApp->KeyAction(event.key.keysym.sym, event.key.state);
//                                        }
                                    }
                                    SDL_PumpEvents();
                                    gApp->GameLoop();

                                    // Swap front and back rendering buffers
                                    mainWindow->Swap();
                                }
                                gApp->Destroy();
                            }
                            else
                            {
                                cout << "Unable to initialize graphics..." << endl;
                            }
                        }
                        else
                        {
                            cout << "Unable to load OpenGL functions..." << endl;
                        }
                        delete mainWindow;
                    }
                    SDL_SetRelativeMouseMode(SDL_FALSE);

                    SDL_Quit();
                }
                else
                {
                    cout << "Unable to initialize SDL..." << endl;
                }
            }
        }
        else
        {
            cout << "Unable to initialize Application..." << endl;
        }
    }
    else
    {
        cout << "No application defined." << endl;
    }
    return 0;
}
