#ifndef APPLICATION_H
#define APPLICATION_H

#include <vector>
#include <string>
#include "../gameactions.h"

class System
{
public:
    System(int argc, char* argv[]);
    virtual ~System();

    /// Gets a vector with the application execution arguments
    std::vector<std::string>& GetArgs() { return _args; }

    /// Gets the time since application start, 1.0 = 1 second
    double GetTime();
private:
    std::vector<std::string> _args;

};

class Application
{
public:
    class Window
    {
    protected:
        Window() { }
    public:
        virtual ~Window() { }

        virtual void MakeCurrent() = 0;
        virtual void Swap() = 0;
    };

public:
    virtual ~Application() { }

    virtual void GetContextAttributes(int& major, int& minor, bool& core) = 0;
    virtual bool IsCommandLineApplication() { return false; }

    virtual bool InitializeApplication(System* sys) = 0;
    virtual bool InitializeGraphics() = 0;
    virtual void GameLoop() = 0;
    virtual bool IsRunning() = 0;
    virtual void Resize(int w, int h) = 0;
    virtual void InputAnalogAction(GameActions action, int x, int y) = 0;
    virtual void InputDigitalAction(GameActions action, int state) = 0;
    virtual void Close() = 0;
    virtual void Destroy() = 0;

    Window* MainWindow();
    Window* AddWindow(const char* title, int width, int height);
    void CloseWindow(Window* window);

};

extern Application* gApp;

#endif // APPLICATION_H
