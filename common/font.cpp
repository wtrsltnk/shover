#include "font.h"
#include "shader.h"

#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

#include <SDL.h>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

const std::string fontVertexShader(
    "#version 140\n"
    "attribute vec3 a_vertex;"
    "attribute vec2 a_texcoord;"
    "uniform mat4 u_projection;"
    "varying vec2 v_texcoord;"
    "void main()"
    "{"
    "    gl_Position = u_projection * vec4(a_vertex.xyz, 1.0);"
    "    v_texcoord = a_texcoord.st;"
    "}");

const std::string fontFragmentShader(
    "#version 140\n"
    "uniform sampler2D u_tex;"
    "uniform vec4 u_global_color;"
    "varying vec2 v_texcoord;"
    "void main()"
    "{"
    "   vec4 t = texture2D(u_tex, v_texcoord.st);"
    "   gl_FragColor = vec4(1.0, 1.0, 1.0, t.a);"
    "}");

bool Font::SetupShader()
{
    if (Font::program == 0)
    {
        std::cout << "Start loading font shaders:" << std::endl;
        Font::program = LoadShaderProgram(fontVertexShader, fontFragmentShader);
        glUseProgram(Font::program);

        Font::a_vertex = glGetAttribLocation(Font::program, "a_vertex");
        Font::a_texcoord = glGetAttribLocation(Font::program, "a_texcoord");
        Font::u_projection = glGetUniformLocation(Font::program, "u_projection");
        Font::u_global_color = glGetUniformLocation(Font::program, "u_global_color");

        return true;
    }

    return false;
}

GLuint Font::program = 0;
GLuint Font::a_vertex = 0;
GLuint Font::a_texcoord = 0;
GLuint Font::u_projection = 0;
GLuint Font::u_tex = 0;
GLuint Font::u_global_color = 0;

#define imagesize 512

Font::Font()
    : tex(0), cdata(nullptr)
{}

Font::~Font()
{
    if (this->cdata != nullptr)
    {
        delete[] this->cdata;
        this->cdata = nullptr;
    }
}

bool Font::Init(const std::string &fontname, float size, int startchar, int endchar)
{
    bool res = false;
    Font::SetupShader();

    this->fontsize = size;
    this->startchar = startchar;
    this->endchar = endchar;

    SDL_RWops *io = SDL_RWFromFile(fontname.c_str(), "rb");
    if (io == NULL)
    {
        std::cout << "Unable to open font file" << std::endl;

        return false;
    }

    auto buffer = new unsigned char[io->size(io) + 1];
    if (io->read(io, buffer, io->size(io), 1) <= 0)
    {
        std::cout << "Unable to read all the bytes from font file" << std::endl;

        return false;
    }

    this->cdata = new stbtt_bakedchar[this->endchar - this->startchar];
    auto bitmap = new unsigned char[imagesize * imagesize];
    stbtt_BakeFontBitmap(buffer, 0, this->fontsize, bitmap, imagesize, imagesize, this->startchar, this->endchar - this->startchar, this->cdata);
    delete[] buffer;

    glGenTextures(1, &(this->tex));
    glBindTexture(GL_TEXTURE_2D, this->tex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, imagesize, imagesize, 0, GL_ALPHA, GL_UNSIGNED_BYTE, bitmap);
    delete[] bitmap;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    res = true;
    io->close(io);

    return res;
}

using namespace std;
void Font::PrintText(const glm::mat4 &ortho, const std::string &text2print, const glm::vec4 &color)
{
    if (text2print.empty())
    {
        return;
    }

    std::vector<FontVertex> res;
    float x = 0, y = 0;
    const char *text = text2print.c_str();
    while (*text)
    {
        if (*text >= 0 && *text < 128)
        {
            stbtt_aligned_quad q;
            stbtt_GetBakedQuad(this->cdata, imagesize, imagesize, *text, &x, &y, &q, 1);
            FontVertex a[] = {
                {{q.x0, q.y0, 0.0f}, {q.s0, q.t0}},
                {{q.x1, q.y0, 0.0f}, {q.s1, q.t0}},
                {{q.x1, q.y1, 0.0f}, {q.s1, q.t1}},
                {{q.x0, q.y1, 0.0f}, {q.s0, q.t1}}};

            res.push_back(a[0]);
            res.push_back(a[1]);
            res.push_back(a[2]);

            res.push_back(a[0]);
            res.push_back(a[2]);
            res.push_back(a[3]);
        }
        ++text;
    }

    glUseProgram(Font::program);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, this->tex);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glUniformMatrix4fv(Font::u_projection, 1, false, glm::value_ptr(ortho));
    glUniform4fv(Font::u_global_color, 1, glm::value_ptr(color));

    glVertexAttribPointer(Font::a_vertex, 3, GL_FLOAT, GL_FALSE, sizeof(FontVertex), (GLvoid *)&(res[0].vertex[0]));
    glEnableVertexAttribArray(Font::a_vertex);

    glVertexAttribPointer(Font::a_texcoord, 2, GL_FLOAT, GL_FALSE, sizeof(FontVertex), (GLvoid *)&(res[0].texcoord[0]));
    glEnableVertexAttribArray(Font::a_texcoord);

    Font::u_tex = glGetUniformLocation(Font::program, "u_tex");
    glUniform1i(Font::u_tex, 0);

    glDrawArrays(GL_TRIANGLES, 0, res.size());
}

glm::vec4 Font::TextBounds(const std::string &text2print)
{
    float x = 0, y = 0;
    float minx = 99999, maxx = -99999, miny = 99999, maxy = -99999;

    const char *text = text2print.c_str();
    while (*text)
    {
        if (*text >= 0 && *text < 128)
        {
            stbtt_aligned_quad q;
            stbtt_GetBakedQuad(this->cdata, imagesize, imagesize, *text, &x, &y, &q, 1);
            if (q.x0 < minx) minx = q.x0;
            if (q.x0 > maxx) maxx = q.x0;

            if (q.y0 < miny) miny = q.y0;
            if (q.y0 > maxy) maxy = q.y0;

            if (q.x1 < minx) minx = q.x1;
            if (q.x1 > maxx) maxx = q.x1;

            if (q.y1 < miny) miny = q.y1;
            if (q.y1 > maxy) maxy = q.y1;
        }
        ++text;
    }

    return glm::vec4(minx, miny, maxx, maxy);
}
