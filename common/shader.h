#ifndef SHADER_H
#define SHADER_H

#include <glad/glad.h>
#include <string>

GLuint LoadShaderProgram(const std::string& vertShaderSrc, const std::string& fragShaderSrc);

#endif // SHADER_H
