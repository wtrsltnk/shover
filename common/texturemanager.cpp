#include "texturemanager.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <iostream>
#include <glad/glad.h>

using namespace std;

TextureManager::TextureManager()
{ }

TextureManager::~TextureManager()
{ }

TextureManager* TextureManager::_sInstance = 0;

TextureManager* TextureManager::CreateInstance()
{
    if (TextureManager::_sInstance == 0)
        TextureManager::_sInstance = new TextureManager();

    return TextureManager::_sInstance;
}

TextureManager* TextureManager::Instance()
{
    return TextureManager::CreateInstance();
}

void TextureManager::DestroyInstance()
{
    if (TextureManager::_sInstance != 0)
        delete TextureManager::_sInstance;
    TextureManager::_sInstance = 0;
}

unsigned int TextureManager::LoadTexture(const std::string &name, int& w, int& h)
{
    // TODO: change this to make sense
    std::string filename = "../data/" + name + ".png";

    return TextureManager::LoadTextureFromFilename(filename, w, h);
}

unsigned int upload(int w, int h, int bpp, stbi_uc* data)
{
    // Only upload when gl index == -1 (this means it's not uploaded yet)
    GLuint glIndex = 0;

    GLenum format = GL_RGB;

    // Determine format
    switch (bpp)
    {
    case 3: format = GL_RGB; break;
    case 4: format = GL_RGBA; break;
    }

    glGenTextures(1, &glIndex);
    glBindTexture(GL_TEXTURE_2D, glIndex);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
    glTexImage2D(GL_TEXTURE_2D, 0, bpp, w, h, 0, format, GL_UNSIGNED_BYTE, data);

    return glIndex;
}

unsigned int TextureManager::LoadTextureFromFilename(const std::string& filename, int& w, int& h)
{
    std::map<std::string, unsigned int>::iterator found = this->_textures.find(filename);
    if (found == this->_textures.end())
    {
        unsigned int textureId;

        int bpp = 0;
        auto diffuse = stbi_load(filename.c_str(), &w, &h, &bpp, 3);
        textureId = upload(w, h, bpp, diffuse);
        delete diffuse;

        this->_textures.insert(std::make_pair(filename, textureId));
        return textureId;
    }
    return found->second;
}
