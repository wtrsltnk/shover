#ifndef FONT_H
#define FONT_H

#include <stdio.h>
#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <glad/glad.h>

#include "stb_truetype.h"
#include "shader.h"

typedef struct
{
    float vertex[3];
    float texcoord[2];

} FontVertex;

class Font
{
    unsigned int startchar;
    unsigned int endchar;
    stbtt_bakedchar *cdata;

public:
    Font();
    virtual ~Font();

    bool Init(const std::string & fontname, float size, int startchar = 0, int endchar = 128);
    void PrintText(const glm::mat4 & ortho, const std::string & text, const glm::vec4& color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
    glm::vec4 TextBounds(const std::string& text);

    float fontsize;
    GLuint tex;

    static bool SetupShader();

    static GLuint program;
    static GLuint a_vertex;
    static GLuint a_texcoord;
    static GLuint u_projection;
    static GLuint u_tex;
    static GLuint u_global_color;
};

#endif				// FONT_H
