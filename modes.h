#ifndef _MODES_H_
#define _MODES_H_

#include "gameactions.h"

class Mode;
class ModeChanger
{
public:
    virtual ~ModeChanger() { }

    virtual void GotoMode(Mode* mode) = 0;
};

class Mode
{
public:
    Mode(ModeChanger* changer) : _changer(changer), _updateTime(0), _renderTime(0) { }
    virtual ~Mode() { }

    virtual void EnterMode(double time, Mode* prev) = 0;
    virtual void LeaveMode(double time, Mode* next) = 0;

    virtual void Update(double time) = 0;
    virtual void Render(double time) = 0;

    virtual void InputAnalogAction(GameActions action, int x, int y) = 0;
    virtual void InputDigitalAction(GameActions action, int state) = 0;

    double RenderTime() { return this->_renderTime; }
    double UpdateTime() { return this->_updateTime; }
protected:
    ModeChanger* _changer;
    double _renderTime;
    double _updateTime;
};

#endif // _MODES_H_
