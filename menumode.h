#ifndef _MENUMODE_H_
#define _MENUMODE_H_

#include "modes.h"
#include "gamemode.h"
#include "common/font.h"

class MenuMode : public Mode
{
public:
    MenuMode(ModeChanger* changer);
    virtual ~MenuMode();

    virtual void EnterMode(double time, Mode* prev);
    virtual void LeaveMode(double time, Mode* next);

    virtual void Update(double time);
    virtual void Render(double time);

    virtual void InputAnalogAction(GameActions  action, int x, int y);
    virtual void InputDigitalAction(GameActions action, int state);

private:
    GameMode* _gamemode;
    Font _monaco;
};

#endif // _MENUMODE_H_
