#ifndef _TREE_H_
#define _TREE_H_

#include "object.h"

class Tree : public GameObject
{
public:
    Tree();
    virtual ~Tree();

    virtual void Render(const glm::mat4& projection, const glm::mat4& view);

};

#endif // _TREE_H_
