#ifndef FLOOR_H
#define FLOOR_H

#include "object.h"
#include <glm/glm.hpp>
#include <glad/glad.h>

class Floor : public GameObject
{
    GLuint _texture;
    unsigned char* _textureData;
    class Car* _car;
public:
    Floor(class Car* car);

    virtual GameObjectTypes type() { return GameObjectTypes::FloorObject; }

    virtual void Tick(double time);
    virtual void Render(const glm::mat4& projection, const glm::mat4& view);

public:
    static GLuint shader();
    static GLint projectionUniform(GLuint shader);
    static GLint viewUniform(GLuint shader);
    static GLint modelUniform(GLuint shader);
    static GLint colorUniform(GLuint shader);
    static GLint textureUniform(GLuint shader);

    void UpdateTexture();
};

#endif // FLOOR_H
