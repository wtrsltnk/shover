#include "trigger.h"
#include "gamemode.h"
#include <iostream>
#include <set>

using namespace std;

Trigger::Trigger()
    : GameObject()
{ }

Trigger::~Trigger()
{ }

void Trigger::OnCollide(const GameObject* other)
{
    std::cout << "trigger" << std::endl;
}
