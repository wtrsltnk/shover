#ifndef _TRIGGER_H_
#define _TRIGGER_H_

#include "object.h"
#include "physics.h"
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <set>

class Trigger : public GameObject
{
public:
    Trigger();
    virtual ~Trigger();

    virtual void OnCollide(const GameObject* other);

};

#endif // _TRADETRIGGER_H_
