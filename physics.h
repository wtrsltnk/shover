/* 
 * File:   Physics.h
 * Author: Administrator
 *
 * Created on 28 oktober 2010, 2:08
 */

#ifndef PHYSICS_H
#define	PHYSICS_H

#include <btBulletDynamicsCommon.h>
#include <LinearMath/btMotionState.h>
#include <LinearMath/btIDebugDraw.h>
#include <BulletDynamics/Dynamics/btRigidBody.h>
#include <vector>
#include <set>
#include <glm/glm.hpp>
#include <iostream>

#include "object.h"
#include "icollisionhandler.h"

class Trigger;

class PhysicsObject : public btMotionState
{
public:
    PhysicsObject(GameObject* obj, float mass);
    virtual ~PhysicsObject();

    glm::vec3 position() const;
	
	virtual void getWorldTransform(btTransform& worldTrans) const;
	virtual void setWorldTransform(const btTransform& worldTrans);

    virtual void UpdateObject(double time) { }
    virtual void InputDigitalAction(GameActions action, int state) { }

    GameObject* _obj;
    btCollisionShape* _shape;
    btTransform _transform;
    btRigidBody* _rigidBody;
};

class PhysicsManager
{
public:
    static struct Config {
        float _gravity;

    } _config;

    PhysicsManager();
    static PhysicsManager* sInstance;
public:
    static PhysicsManager* Instance();
    virtual ~PhysicsManager();

    void Update(float gameTime);
    PhysicsObject* AddObject(GameObject* obj, float mass = 0.0f, short group = btBroadphaseProxy::DefaultFilter, short mask = btBroadphaseProxy::DefaultFilter | btBroadphaseProxy::StaticFilter | btBroadphaseProxy::CharacterFilter);
    void RemoveObject(GameObject* obj);
//private:
    btBroadphaseInterface* _broadphase;
    btDefaultCollisionConfiguration* _collisionConfiguration;
    btCollisionDispatcher* _dispatcher;
    btSequentialImpulseConstraintSolver* _solver;
    btDiscreteDynamicsWorld* _dynamicsWorld;
//    std::set<ICollisionHandler*> _collisionHandlers;
    ICollisionHandler* _collisionHandlers;
};

#endif	/* PHYSICS_H */

