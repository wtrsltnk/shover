#include "meshfactory.h"
#include "object.h"
#include "trigger.h"
#include <glad/glad.h>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <vector>

using namespace std;

void addTriangleToList(const glm::vec3 &a, const glm::vec2 &uva,
                       const glm::vec3 &b, const glm::vec2 &uvb,
                       const glm::vec3 &c, const glm::vec2 &uvc,
                       std::vector<Vertex> &list)
{
    auto normal = glm::normalize(glm::cross(b - a, c - a));

    auto v1 = Vertex({a, normal, uva});
    auto v2 = Vertex({b, normal, uvb});
    auto v3 = Vertex({c, normal, uvc});

    list.push_back(v1);
    list.push_back(v2);
    list.push_back(v3);
}

PhysicsMesh *MeshFactory::CreateMesh(const btCollisionShape *shape, const glm::mat4 &m)
{
    std::vector<Vertex> verts;
    std::vector<Mesh> meshes;
    std::vector<PhysicsMesh *> children;

    if (shape->getShapeType() == COMPOUND_SHAPE_PROXYTYPE)
    {
        const btCompoundShape *compound = (btCompoundShape *)shape;
        for (int i = 0; i < compound->getNumChildShapes(); i++)
        {
            glm::mat4 m;
            compound->getChildTransform(i).getOpenGLMatrix(glm::value_ptr(m));
            children.push_back(MeshFactory::CreateMesh((const btCollisionShape *)compound->getChildShape(i), m));
        }
    }
    else if (shape->getShapeType() == BOX_SHAPE_PROXYTYPE)
    {
        btBoxShape *box = (btBoxShape *)shape;
        btVector3 v = box->getHalfExtentsWithoutMargin();

        // Front
        addTriangleToList(glm::vec3(v.x(), v.y(), v.z()), glm::vec2(1.0f, 1.0f),
                          glm::vec3(v.x(), -v.y(), v.z()), glm::vec2(1.0f, 0.0f),
                          glm::vec3(-v.x(), v.y(), v.z()), glm::vec2(0.0f, 1.0f),
                          verts);
        addTriangleToList(glm::vec3(-v.x(), -v.y(), v.z()), glm::vec2(0.0f, 0.0f),
                          glm::vec3(-v.x(), v.y(), v.z()), glm::vec2(0.0f, 1.0f),
                          glm::vec3(v.x(), -v.y(), v.z()), glm::vec2(1.0f, 0.0f),
                          verts);

        // Back
        addTriangleToList(glm::vec3(-v.x(), v.y(), -v.z()), glm::vec2(0.0f, 1.0f),
                          glm::vec3(v.x(), -v.y(), -v.z()), glm::vec2(1.0f, 0.0f),
                          glm::vec3(v.x(), v.y(), -v.z()), glm::vec2(1.0f, 1.0f),
                          verts);
        addTriangleToList(glm::vec3(v.x(), -v.y(), -v.z()), glm::vec2(1.0f, 0.0f),
                          glm::vec3(-v.x(), v.y(), -v.z()), glm::vec2(0.0f, 1.0f),
                          glm::vec3(-v.x(), -v.y(), -v.z()), glm::vec2(0.0f, 0.0f),
                          verts);

        // Right
        addTriangleToList(glm::vec3(v.x(), v.y(), -v.z()), glm::vec2(1.0f, 0.0f),
                          glm::vec3(v.x(), -v.y(), v.z()), glm::vec2(0.0f, 1.0f),
                          glm::vec3(v.x(), v.y(), v.z()), glm::vec2(1.0f, 1.0f),
                          verts);
        addTriangleToList(glm::vec3(v.x(), -v.y(), v.z()), glm::vec2(0.0f, 1.0f),
                          glm::vec3(v.x(), v.y(), -v.z()), glm::vec2(1.0f, 0.0f),
                          glm::vec3(v.x(), -v.y(), -v.z()), glm::vec2(0.0f, 0.0f),
                          verts);

        // Left
        addTriangleToList(glm::vec3(-v.x(), v.y(), v.z()), glm::vec2(1.0f, 1.0f),
                          glm::vec3(-v.x(), -v.y(), v.z()), glm::vec2(0.0f, 1.0f),
                          glm::vec3(-v.x(), v.y(), -v.z()), glm::vec2(1.0f, 0.0f),
                          verts);
        addTriangleToList(glm::vec3(-v.x(), -v.y(), -v.z()), glm::vec2(0.0f, 0.0f),
                          glm::vec3(-v.x(), v.y(), -v.z()), glm::vec2(1.0f, 0.0f),
                          glm::vec3(-v.x(), -v.y(), v.z()), glm::vec2(0.0f, 1.0f),
                          verts);

        // Top
        addTriangleToList(glm::vec3(v.x(), v.y(), v.z()), glm::vec2(1.0f, 1.0f),
                          glm::vec3(-v.x(), v.y(), v.z()), glm::vec2(0.0f, 1.0f),
                          glm::vec3(v.x(), v.y(), -v.z()), glm::vec2(1.0f, 0.0f),
                          verts);
        addTriangleToList(glm::vec3(-v.x(), v.y(), -v.z()), glm::vec2(0.0f, 0.0f),
                          glm::vec3(v.x(), v.y(), -v.z()), glm::vec2(1.0f, 0.0f),
                          glm::vec3(-v.x(), v.y(), v.z()), glm::vec2(0.0f, 1.0f),
                          verts);

        // Bottom
        addTriangleToList(glm::vec3(v.x(), -v.y(), -v.z()), glm::vec2(1.0f, 0.0f),
                          glm::vec3(-v.x(), -v.y(), v.z()), glm::vec2(0.0f, 1.0f),
                          glm::vec3(v.x(), -v.y(), v.z()), glm::vec2(1.0f, 1.0f),
                          verts);
        addTriangleToList(glm::vec3(-v.x(), -v.y(), v.z()), glm::vec2(0.0f, 1.0f),
                          glm::vec3(v.x(), -v.y(), -v.z()), glm::vec2(1.0f, 0.0f),
                          glm::vec3(-v.x(), -v.y(), -v.z()), glm::vec2(0.0f, 0.0f),
                          verts);

        Mesh mesh;
        mesh._mode = GL_TRIANGLES;
        mesh._first = 0;
        mesh._count = verts.size();
        meshes.push_back(mesh);
    }
    else if (shape->getShapeType() == CAPSULE_SHAPE_PROXYTYPE)
    {
        btCylinderShape *cyl = (btCylinderShape *)shape;
        btVector3 v = cyl->getHalfExtentsWithoutMargin();

        const float PI = 3.14159f;
        float step = 0.314159f * 2.0f;

        for (float angle = 0; angle <= 2 * PI; angle += step)
        {
            float x1 = (v.x()) * cos(angle);
            float z1 = (v.z()) * sin(angle);
            float x2 = (v.x()) * cos(angle + step);
            float z2 = (v.z()) * sin(angle + step);

            addTriangleToList(glm::vec3(x1, v.y(), z1), glm::vec2(1.0f, 1.0f),
                              glm::vec3(x1, -v.y(), z1), glm::vec2(1.0f, 1.0f),
                              glm::vec3(x2, v.y(), z2), glm::vec2(1.0f, 1.0f),
                              verts);

            addTriangleToList(glm::vec3(x1, -v.y(), z1), glm::vec2(1.0f, 1.0f),
                              glm::vec3(x2, -v.y(), z2), glm::vec2(1.0f, 1.0f),
                              glm::vec3(x2, v.y(), z2), glm::vec2(1.0f, 1.0f),
                              verts);

            addTriangleToList(glm::vec3(x2, -v.y(), z2), glm::vec2(1.0f, 1.0f),
                              glm::vec3(x1, -v.y(), z1), glm::vec2(1.0f, 1.0f),
                              glm::vec3(0.0f, -v.y(), 0.0f), glm::vec2(1.0f, 1.0f),
                              verts);

            addTriangleToList(glm::vec3(x1, v.y(), z1), glm::vec2(1.0f, 1.0f),
                              glm::vec3(x2, v.y(), z2), glm::vec2(1.0f, 1.0f),
                              glm::vec3(0.0f, v.y(), 0.0f), glm::vec2(1.0f, 1.0f),
                              verts);
        }

        Mesh mesh;
        mesh._mode = GL_TRIANGLES;
        mesh._first = 0;
        mesh._count = verts.size();
        meshes.push_back(mesh);
    }
    else if (shape->getShapeType() == CONE_SHAPE_PROXYTYPE)
    {
        btConeShape *cyl = (btConeShape *)shape;
        auto radius = cyl->getRadius(); // not sure why, this probably returns diagonal
        auto height = cyl->getHeight();

        const float PI = 3.14159f;
        float step = 0.314159f * 2.0f;

        for (float angle = 0; angle <= 2 * PI; angle += step)
        {
            float x1 = radius * cos(angle);
            float z1 = radius * sin(angle);
            float x2 = radius * cos(angle + step);
            float z2 = radius * sin(angle + step);

            addTriangleToList(glm::vec3(x1, 0.0f - (height / 2.0f), z1), glm::vec2(1.0f, 1.0f),
                              glm::vec3(x2, 0.0f - (height / 2.0f), z2), glm::vec2(1.0f, 1.0f),
                              glm::vec3(0.0f, height - (height / 2.0f), 0.0f), glm::vec2(1.0f, 1.0f),
                              verts);
        }

        Mesh mesh;
        mesh._mode = GL_TRIANGLES;
        mesh._first = 0;
        mesh._count = verts.size();
        meshes.push_back(mesh);
    }
    else if (shape->getShapeType() == SPHERE_SHAPE_PROXYTYPE)
    {
        btSphereShape *sphere = (btSphereShape *)shape;

        float fRadius = sphere->getRadius();
        int iStacks = 2, iSlices = 2;
        GLfloat drho = 3.141592653589f / (GLfloat)iStacks;
        GLfloat dtheta = 2.0f * 3.141592653589f / (GLfloat)iSlices;
        GLfloat ds = 1.0f / (GLfloat)iSlices;
        GLfloat dt = 1.0f / (GLfloat)iStacks;
        GLfloat t = 1.0f;
        GLfloat s = 0.0f;
        GLint i, j; // Looping variables

        for (i = 0; i < iStacks; i++)
        {
            GLfloat rho = (GLfloat)i * drho;
            GLfloat srho = (GLfloat)(sin(rho));
            GLfloat crho = (GLfloat)(cos(rho));
            GLfloat srhodrho = (GLfloat)(sin(rho + drho));
            GLfloat crhodrho = (GLfloat)(cos(rho + drho));

            s = 0.0f;
            for (j = 0; j <= iSlices; j++)
            {
                GLfloat theta = (j == iSlices) ? 0.0f : j * dtheta;
                GLfloat stheta = (GLfloat)(-sin(theta));
                GLfloat ctheta = (GLfloat)(cos(theta));

                GLfloat x = stheta * srhodrho;
                GLfloat z = ctheta * srhodrho;
                GLfloat y = crhodrho;

                //                glTexCoord2f(s, t);
                verts.push_back(Vertex({glm::vec3(x * fRadius, y * fRadius, z * fRadius), glm::vec3(x, y, z)}));

                x = stheta * srho;
                z = ctheta * srho;
                y = crho;
                //                glTexCoord2f(s, t - dt);
                verts.push_back(Vertex({glm::vec3(x * fRadius, y * fRadius, z * fRadius), glm::vec3(x, y, z)}));
            }

            t -= dt;
        }

        Mesh mesh;
        mesh._mode = GL_TRIANGLE_STRIP;
        mesh._first = 0;
        mesh._count = verts.size();
        meshes.push_back(mesh);
    }

    if (verts.size() == 0)
    {
        return nullptr;
    }

    auto result = new PhysicsMesh(verts, meshes, m);
    result->Children().insert(result->Children().end(), children.begin(), children.end());
    return result;
}
