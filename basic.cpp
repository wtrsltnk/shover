#include "basic.h"
#include "gamemode.h"
#include <btBulletDynamicsCommon.h>
#include <glad/glad.h>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <sstream>

using namespace std;

Application *gApp = new Basic();

Basic::Basic()
    : _running(true), _mode(0)
{}

Basic::~Basic()
{}

bool Basic::InitializeApplication(System *sys)
{
    this->_sys = sys;
    return true;
}

void Basic::GotoMode(Mode *mode)
{
    if (mode != 0)
    {
        if (this->_mode != 0)
            this->_mode->LeaveMode(this->_sys->GetTime(), mode);
        mode->EnterMode(this->_sys->GetTime(), this->_mode);
        this->_mode = mode;
    }
}

static glm::mat4 fpsMatrix;
bool Basic::InitializeGraphics()
{
    cout << "GL_VERSION                  : " << glGetString(GL_VERSION) << endl;
    cout << "GL_SHADING_LANGUAGE_VERSION : " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
    cout << "GL_RENDERER                 : " << glGetString(GL_RENDERER) << endl;
    cout << "GL_VENDOR                   : " << glGetString(GL_VENDOR) << endl;
    glClearColor(0.1f, 0.5f, 0.8f, 1.0f);

    this->_mode = this->_menu = new MenuMode(this);

    if (!this->_monaco.Init("../shover/monaco.ttf", 16.0f))
    {
        return false;
    }

    int width = 800, height = 600;
    fpsMatrix = glm::translate(glm::ortho(0.0f, float(width), float(height), 0.0f, -10.0f, 10.0f), glm::vec3(width - 80.0f, 16.0f, 0.0f));

    return true;
}

static int fps = 0;
static double lastfpsupdate = 0.0;
static char fpsString[64];

static double lastupdate = 0.0;
void Basic::GameLoop()
{
    double time = this->_sys->GetTime();

    if ((time - lastupdate) > (1.0 / 120.0))
    {
        this->_mode->Update(time - lastupdate);

        lastupdate = time; //+= (1.0 / 60.0);
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLineWidth(1.0f);

    fps++;
    this->_mode->Render(time - this->_mode->RenderTime());

    if ((time - lastfpsupdate) > 1.0)
    {
        _itoa_s(fps, fpsString, 64, 10);
        lastfpsupdate = time;
        fps = 0;
    }

    this->_monaco.PrintText(fpsMatrix, fpsString);
}

bool Basic::IsRunning()
{
    return this->_running;
}

void Basic::Resize(int w, int h)
{
    if (h <= 0) h = 1;
    if (w <= 0) w = 1;

    glViewport(0, 0, w, h);

    ortho = glm::ortho(0.0f, float(w), float(h), 0.0f, -10.0f, 10.0f);
}

void Basic::InputAnalogAction(GameActions action, int x, int y)
{
    this->_mode->InputAnalogAction(action, x, y);
}

void Basic::InputDigitalAction(GameActions action, int state)
{
    this->_mode->InputDigitalAction(action, state);
}

void Basic::Close()
{
    this->_running = false;
}

void Basic::Destroy()
{}
