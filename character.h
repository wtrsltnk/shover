#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include "physics.h"
#include "object.h"

class Car;

class Character : public GameObject
{
public:
    class Physics : public PhysicsObject
    {
    public:
        Physics(GameObject* obj, float mass);
        virtual ~Physics();

        virtual void UpdateObject(double time);
        virtual void InputDigitalAction(GameActions action, int state);

        void MoveCharacter(float left, float forward);

        float _forward;
        float _left;
    };

    Car* _car;

public:
    Character();
    virtual ~Character();

    virtual PhysicsObject* CreatePhysics(float mass);
    virtual void Render(const glm::mat4& projection, const glm::mat4& view);

    virtual GameObjectTypes type() { return GameObjectTypes::CharacterObject; }

    virtual const char* Id() { return "Character"; }
    virtual void InputDigitalAction(GameActions action, int state);

    Character::Physics* GetCharacterPhysics() { return (Character::Physics*)this->_phys; }

    void GetInCar(Car* car);
    Car* GetOutOfCar();
};

#endif // _CHARACTER_H_
