#include "mesh.h"
#include "object.h"
#include "trigger.h"
#include "common/shader.h"
#include <iostream>
#include <vector>
#include <glm/gtc/type_ptr.hpp>

using namespace std;

PhysicsMesh::PhysicsMesh(const std::vector<Vertex>& verts, const std::vector<Mesh>& meshes, const glm::mat4& relativeToParent)
    : _vao(0), _vbo(0), _relativeToParent(relativeToParent), _color(glm::vec4(1.0f))
{
    this->_meshes = meshes;

    if (this->_vao == 0)
        glGenVertexArrays(1, &this->_vao);
    glBindVertexArray(this->_vao);

    if (this->_vbo == 0)
        glGenBuffers(1, &this->_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo);

    auto vertexAttrib = glGetAttribLocation(PhysicsMesh::shader(), "vertex");
    glVertexAttribPointer(GLuint(vertexAttrib), 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
    glEnableVertexAttribArray(GLuint(vertexAttrib));

    auto normalAttrib = glGetAttribLocation(PhysicsMesh::shader(), "normal");
    glVertexAttribPointer(GLuint(normalAttrib), 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<const GLvoid*>(sizeof(float) * 3));
    glEnableVertexAttribArray(GLuint(normalAttrib));

    auto uvAttrib = glGetAttribLocation(PhysicsMesh::shader(), "uv");
    glVertexAttribPointer(GLuint(uvAttrib), 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<const GLvoid*>(sizeof(float) * 6));
    glEnableVertexAttribArray(GLuint(uvAttrib));

    glBufferData(GL_ARRAY_BUFFER, GLsizeiptr(verts.size() * sizeof(Vertex)), 0, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, GLsizeiptr(verts.size() * sizeof(Vertex)), reinterpret_cast<const GLvoid*>(&verts[0].v[0]));

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

PhysicsMesh::~PhysicsMesh()
{
    if (this->_vbo != 0)
        glDeleteBuffers(1, &this->_vbo);
    this->_vbo = 0;

    if (this->_vao != 0)
        glDeleteVertexArrays(1, &this->_vao);
    this->_vao = 0;
}

void PhysicsMesh::Render(const glm::mat4& proj, const glm::mat4& view, const glm::mat4& model)
{
    auto m = model * this->_relativeToParent;

    if (this->_vao != 0 && this->_vbo != 0)
    {
        glUseProgram(PhysicsMesh::shader());
        glUniformMatrix4fv(PhysicsMesh::projectionUniform(PhysicsMesh::shader()), 1, false, glm::value_ptr(proj));
        glUniformMatrix4fv(PhysicsMesh::viewUniform(PhysicsMesh::shader()), 1, false, glm::value_ptr(view));
        glUniformMatrix4fv(PhysicsMesh::modelUniform(PhysicsMesh::shader()), 1, false, glm::value_ptr(m));
        glUniform4fv(PhysicsMesh::colorUniform(PhysicsMesh::shader()), 1, glm::value_ptr(this->_color));

        this->BindBuffers();
        this->DrawVertices();
        this->UnbindBuffers();
    }

    for (auto child : this->_children)
        child->Render(proj, view, m);
}

void PhysicsMesh::BindBuffers()
{
    glBindVertexArray(this->_vao);
}

void PhysicsMesh::DrawVertices()
{
    for (auto mesh : this->_meshes)
        glDrawArrays(mesh._mode, mesh._first, mesh._count);

}

void PhysicsMesh::UnbindBuffers()
{
    glBindVertexArray(0);
}

void PhysicsMesh::setColor(const glm::vec4& color)
{
    this->_color = color;
    for (auto child : this->_children)
        child->setColor(color);
}

const glm::vec4& PhysicsMesh::color() const
{
    return this->_color;
}

std::vector<PhysicsMesh*>& PhysicsMesh::Children()
{
    return this->_children;
}

static std::string vertexShader = std::string(
    "#version 140\n"

    "uniform mat4 u_p;"
    "uniform mat4 u_v;"
    "uniform mat4 u_m;"

    "in vec3 vertex;"
    "in vec3 normal;"
    "in vec2 uv;"

    "smooth out vec4 shadeColor;"

    "void main()"
    "{"
    "   gl_Position = u_p * u_v * u_m * vec4(vertex, 1.0);"

    // Light color
    "    mat4 normalMatrix = transpose(inverse(u_v * u_m));"
    "    vec4 lightNor = normalize(normalMatrix * vec4(normal.xyz, 1.0));"
    "    vec3 lightDir1 = normalize(vec3(3.0, 1.0, -2.0));"
    "    float NdotL1 = clamp(dot(lightNor.xyz, lightDir1.xyz), 0, 1);"
    "    shadeColor = (NdotL1 * vec4(0.4, 0.5, 0.6, 1.0)) + vec4(0.6, 0.6, 0.6, 1.0);"
    "}"
);

static std::string fragmentShader = std::string(
    "#version 140\n"

    "smooth in vec4 shadeColor;"
    "uniform vec4 u_color;"
    "out vec4 colorOut;"

    "void main()"
    "{"
    "   colorOut = u_color * shadeColor;"
    "}"
);

static GLuint geomShader = 0;

GLuint PhysicsMesh::shader()
{
    if (geomShader == 0)
        geomShader = LoadShaderProgram(vertexShader, fragmentShader);

    return geomShader;
}

static GLint _projectionUniform = 0;

GLint PhysicsMesh::projectionUniform(GLuint shader)
{
    if (_projectionUniform == 0)
        _projectionUniform = glGetUniformLocation(shader, "u_p");

    return _projectionUniform;
}

static GLint _viewUniform = 0;

GLint PhysicsMesh::viewUniform(GLuint shader)
{
    if (_viewUniform == 0)
        _viewUniform = glGetUniformLocation(shader, "u_v");

    return _viewUniform;
}

static GLint _modeluniform = 0;

GLint PhysicsMesh::modelUniform(GLuint shader)
{
    if (_modeluniform == 0)
        _modeluniform = glGetUniformLocation(shader, "u_m");

    return _modeluniform;
}

static GLint _coloruniform = 0;

GLint PhysicsMesh::colorUniform(GLuint shader)
{
    if (_coloruniform == 0)
        _coloruniform = glGetUniformLocation(shader, "u_color");

    return _coloruniform;
}
