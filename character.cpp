#include "character.h"
#include <iostream>
#include <SDL.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include "car.h"

using namespace std;

Character::Character()
    : GameObject(), _car(0)
{ }

Character::~Character()
{ }

PhysicsObject* Character::CreatePhysics(float mass)
{
    return new Character::Physics(this, mass);
}

void Character::Render(const glm::mat4& projection, const glm::mat4& view)
{
    GameObject::Render(projection, view);
}

class CustomRayResultCallback : public btCollisionWorld::RayResultCallback
{
public:
    CustomRayResultCallback() : _car(0) { }

    Car* _car;
    virtual	btScalar addSingleResult(btCollisionWorld::LocalRayResult& rayResult,bool normalInWorldSpace)
    {
        if (rayResult.m_collisionObject != 0)
        {
            GameObject* obj = (GameObject*)rayResult.m_collisionObject->getUserPointer();
            if (obj->type() == GameObjectTypes::CarObject)
            {
                this->_car = (Car*)obj;
            }
        }
        return 0;
    }
};

void Character::GetInCar(Car* car)
{
    if (car != 0)
    {
        this->_car = car;
        this->_visible = false;
        PhysicsManager::Instance()->_dynamicsWorld->removeRigidBody(this->_phys->_rigidBody);
    }
}

Car* Character::GetOutOfCar()
{
    Car* car = this->_car;

    if (car != nullptr)
    {
        car->StopCar();
        btTransform t = this->_car->_phys->_rigidBody->getWorldTransform();
        t.setOrigin(t.getOrigin() + (t.getBasis().getColumn(0)*(this->_car->_boundingParameters[0]+this->_boundingParameters[0])));
        this->_phys->_rigidBody->setWorldTransform(t);
        this->_car = 0;
        this->_visible = true;
        PhysicsManager::Instance()->_dynamicsWorld->addRigidBody(this->_phys->_rigidBody);
    }
    return car;
}

void Character::InputDigitalAction(GameActions action, int state)
{
    if (this->_car != 0)
    {
        if (action == GameActions::EnterOrLeaveCar && state == 1)
        {
            this->GetOutOfCar();
        }
        else
            this->_car->InputDigitalAction(action, state);
    }
    else
    {
        if (action == GameActions::EnterOrLeaveCar && state == 1)
        {
            btVector3 from(this->_phys->_rigidBody->getWorldTransform().getOrigin());
            btVector3 to(this->_phys->_rigidBody->getWorldTransform().getOrigin());
            to = to + (this->_phys->_rigidBody->getWorldTransform().getBasis().getColumn(0) * -10);

            CustomRayResultCallback result;

            PhysicsManager::Instance()->_dynamicsWorld->rayTest(from, to, result);
            this->GetInCar(result._car);
        }
        else
        {
            if (this->_phys != 0)
                this->_phys->InputDigitalAction(action, state);
        }
    }
}

Character::Physics::Physics(GameObject* obj, float mass)
    : PhysicsObject(obj, mass)
{
    this->_left = 0;
    this->_forward = 0;

    this->_rigidBody->setActivationState(DISABLE_DEACTIVATION);
}

Character::Physics::~Physics()
{ }

void Character::Physics::UpdateObject(double time)
{
    this->MoveCharacter(-this->_left, -this->_forward);
}

void Character::Physics::InputDigitalAction(GameActions action, int state)
{
    if (action == GameActions::Up && state == 1)
    {
        this->_forward = 1;
    }
    else if (action == GameActions::Up && state == 0)
    {
        this->_forward = 0;
    }
    else if (action == GameActions::Down && state == 1)
    {
        this->_forward = -1;
    }
    else if (action == GameActions::Down && state == 0)
    {
        this->_forward = 0;
    }
    else if (action == GameActions::Left && state == 1)
    {
        this->_left = 1;
    }
    else if (action == GameActions::Left && state == 0)
    {
        this->_left = 0;
    }
    else if (action == GameActions::Right && state == 1)
    {
        this->_left = -1;
    }
    else if (action == GameActions::Right && state == 0)
    {
        this->_left = 0;
    }
}

void Character::Physics::MoveCharacter(float left, float forward)
{
    float speed = 10.0f;
    if (this->_rigidBody != 0)
    {
        static btVector3 direction(0.0f, 0.0f, 1.0f);
        if (left != 0 || forward != 0)
            direction = btVector3(forward, 0.0f, left).normalize();

        btVector3 up(0.0f, -1.0f, 0.0f);
        btVector3 side = direction.cross(up);
        up = direction.cross(side);
        btMatrix3x3 m;
        m[0] = side;
        m[1] = up;
        m[2] = direction;
        btQuaternion qRot;
        m.getRotation(qRot);
        static btQuaternion currRot = qRot;
        btTransform t = this->_rigidBody->getWorldTransform();
        t.setRotation(currRot = currRot.slerp(qRot, 0.1f));
        this->_rigidBody->setWorldTransform(t);

        if (left != 0 || forward != 0)
        {
            btVector3 velocity = this->_rigidBody->getLinearVelocity();
            btVector3 v = btVector3(-left * speed, velocity.y(), -forward * speed);
            this->_rigidBody->setLinearVelocity(v);
        }
    }
}
