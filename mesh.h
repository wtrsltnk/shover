/* 
 * File:   Physics.h
 * Author: Administrator
 *
 * Created on 28 oktober 2010, 2:08
 */

#ifndef MESH_H
#define	MESH_H

#include <vector>
#include <set>
#include <glm/glm.hpp>
#include <glad/glad.h>

typedef struct sVertex
{
    glm::vec3 v;
    glm::vec3 n;
    glm::vec2 uv;

} Vertex;

typedef struct sMesh
{
    unsigned int _mode;
    int _first;
    int _count;

} Mesh;

class PhysicsMesh
{
public:
    PhysicsMesh(const std::vector<Vertex>& verts, const std::vector<Mesh>& meshes, const glm::mat4& relativeToParent = glm::mat4(1.0f));
    virtual ~PhysicsMesh();

    virtual void Render(const glm::mat4& proj, const glm::mat4& view, const glm::mat4& model);
    virtual void BindBuffers();
    virtual void DrawVertices();
    virtual void UnbindBuffers();

    void setColor(const glm::vec4& color);
    const glm::vec4& color() const;

    std::vector<PhysicsMesh*>& Children();
private:
    unsigned int _vao;
    unsigned int _vbo;
    std::vector<PhysicsMesh*> _children;
    std::vector<Mesh> _meshes;
    glm::mat4 _relativeToParent;
    glm::vec4 _color;

public:
    static GLuint shader();
    static GLint projectionUniform(GLuint shader);
    static GLint viewUniform(GLuint shader);
    static GLint modelUniform(GLuint shader);
    static GLint colorUniform(GLuint shader);

};

#endif	/* MESH_H */

