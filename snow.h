#ifndef SNOW_H
#define SNOW_H

#include "object.h"
#include "mesh.h"
#include <glm/glm.hpp>
#include <vector>
#include <btBulletDynamicsCommon.h>

class Snow : public GameObject
{
public:
    class Flake : public GameObject, public btMotionState
    {
        btTransform _transform;
    public:
        Flake(const glm::vec3& position);

        virtual GameObjectTypes type() { return GameObjectTypes::SnowFlakeObject; }

        virtual void getWorldTransform(btTransform& worldTrans) const;
        virtual void setWorldTransform(const btTransform& worldTrans);

        glm::vec3 position() const;
    };

    std::set<Snow::Flake*> _flakes;
public:
    Snow(const std::vector<glm::vec3>& points);

    virtual GameObjectTypes type() { return GameObjectTypes::SnowObject; }

    void Render(const glm::mat4& projection, const glm::mat4& view);

    PhysicsMesh* mesh();
};

#endif // SNOW_H
