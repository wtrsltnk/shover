#ifndef ICOLLISIONHANDLER_H
#define ICOLLISIONHANDLER_H

#include "object.h"

class ICollisionHandler
{
public:
    virtual ~ICollisionHandler();
    virtual bool handleCollision(GameObject* a, GameObject* b) = 0;
};

#endif // ICOLLISIONHANDLER_H
