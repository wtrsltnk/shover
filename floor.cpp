#include "floor.h"
#include "physics.h"
#include "mesh.h"
#include "car.h"
#include "common/shader.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

Floor::Floor(Car* car)
    : _car(car)
{
    this->_textureData = new unsigned char[1024 * 1024];
    for (int y = 0; y < 1024; y++)
    {
        for (int x = 0; x < 1024; x++)
        {
            // Each cell is 8x8, value is 0 or 255 (black or white)
            this->_textureData[y * 1024 + x] = (((y & 0x8) == 0) ^ ((x & 0x8) == 0)) * 255;
        }
    }

    glGenTextures(1, &this->_texture);
    glBindTexture(GL_TEXTURE_2D, this->_texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
    this->UpdateTexture();
}

void Floor::UpdateTexture()
{
    glBindTexture(GL_TEXTURE_2D, this->_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1024, 1024, 0, GL_BLUE, GL_UNSIGNED_BYTE, this->_textureData);
}

void Floor::Tick(double time)
{
    if (this->_car != nullptr)
    {
        auto pos = this->_car->GetCarPhysics()->position();
        this->_textureData[int(pos.z + 512) * 1024 + int(pos.x + 512)] = 155;
        this->UpdateTexture();
    }
}

void Floor::Render(const glm::mat4& projection, const glm::mat4& view)
{
    auto shader = Floor::shader();
    auto m = glm::mat4(1.0f);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, this->_texture);

    this->_mesh->BindBuffers();

    glUseProgram(shader);
    glUniformMatrix4fv(Floor::projectionUniform(shader), 1, false, glm::value_ptr(projection));
    glUniformMatrix4fv(Floor::viewUniform(shader), 1, false, glm::value_ptr(view));
    glUniformMatrix4fv(Floor::modelUniform(shader), 1, false, glm::value_ptr(m));
    glUniform4fv(Floor::colorUniform(shader), 1, glm::value_ptr(glm::vec4(1.0f)));
    glUniform1i(Floor::textureUniform(shader), 0);

    this->_mesh->DrawVertices();

    this->_mesh->UnbindBuffers();
    glDisable(GL_TEXTURE_2D);
}

static std::string vertexShader = std::string(
            "#version 150\n"

            "uniform mat4 u_p;"
            "uniform mat4 u_v;"
            "uniform mat4 u_m;"

            "in vec3 vertex;"
            "in vec3 normal;"
            "in vec2 uv;"

            "out vec4 f_shade;"
            "out vec2 f_uv;"

            "void main()"
            "{"
            // Position
            "    gl_Position = u_p * u_v * u_m * vec4(vertex.xyz, 1.0);"

            // Texcoords
            "    f_uv = uv;"

            // Light color
            "    mat4 normalMatrix = transpose(inverse(u_v * u_m));"
            "    vec4 lightNor = normalize(normalMatrix * vec4(normal.xyz, 1.0));"
            "    vec3 lightDir1 = normalize(vec3(3.0, 1.0, -2.0));"
            "    float NdotL1 = clamp(dot(lightNor.xyz, lightDir1.xyz), 0, 1);"
            "    f_shade = (NdotL1 * vec4(0.4, 0.5, 0.6, 1.0)) + vec4(0.6, 0.6, 0.6, 1.0);"
            "}"
);

static std::string fragmentShader = std::string(
            "#version 150\n"

            "in vec4 f_shade;"
            "in vec2 f_uv;"
            "uniform sampler2D u_texture;"
            "uniform vec4 u_color;"
            "out vec4 colorOut;"

            "void main()"
            "{"
            "   colorOut = vec4(0.6, 0.6, 0.6, 1.0);" //u_color * f_shade * texture(u_texture, f_uv);"
            "}"
);

static GLuint geomShader = 0;

GLuint Floor::shader()
{
    if (geomShader == 0)
        geomShader = LoadShaderProgram(vertexShader, fragmentShader);

    return geomShader;
}

static GLint _projectionUniform = 0;

GLint Floor::projectionUniform(GLuint shader)
{
    if (_projectionUniform == 0)
        _projectionUniform = glGetUniformLocation(shader, "u_p");

    return _projectionUniform;
}

static GLint _viewUniform = 0;

GLint Floor::viewUniform(GLuint shader)
{
    if (_viewUniform == 0)
        _viewUniform = glGetUniformLocation(shader, "u_v");

    return _viewUniform;
}

static GLint _modeluniform = 0;

GLint Floor::modelUniform(GLuint shader)
{
    if (_modeluniform == 0)
        _modeluniform = glGetUniformLocation(shader, "u_m");

    return _modeluniform;
}

static GLint _coloruniform = 0;

GLint Floor::colorUniform(GLuint shader)
{
    if (_coloruniform == 0)
        _coloruniform = glGetUniformLocation(shader, "u_color");

    return _coloruniform;
}

static GLint _textureuniform = 0;

GLint Floor::textureUniform(GLuint shader)
{
    if (_textureuniform == 0)
        _textureuniform = glGetUniformLocation(shader, "u_texture");

    return _textureuniform;
}
