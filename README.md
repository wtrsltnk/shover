# Features van de game: #

* 3D wereld met straten, huizen
* rond lopen door de wereld
* andere auto's in de wereld
* andere voetgangers in de wereld
* politie auto's die achter je aan komen wanneer je betrapt wordt
    * wanneer het auto alarm afgaat met iemand binen een bepaalde straal om de auto wordt de politie gebeld.
    * wanneer het auto alarm afgaat met de politie binnen een grotere straal om de auto, komt deze je achterna.
    * wanneer een auto als gejat opgegeven is en je komt een politie auto tegen.
    * wanneer een auto als gejat op social media staat en iemand in jou buurt herkent de auto en belt de politie.


* wanneer je tegen een auto aanloopt en op de enter druk, probeer je in de auto te gaan zitten(door te stelen of zomaar in te stappen)
* er zijn garages waar je auto's in kunt zetten. Iedere garage heeft één of meer points-in-space waar een auto kan staan. Dit zijn de 'slots' van de garage.
* wanneer je een garage binnen rijdt, 'slot' je de auto(als de garage niet vol is). Dit betekend dat de auto in de garage staat, en je met deze auto kunt gaan handelen. Je krijgt dan ook het "Handel" scherm te zien. Door esc te klikken, ga je weer naar de wereld.

## Screenshot

![Screenshot](screenshot01.png)