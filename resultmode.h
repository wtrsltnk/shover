#ifndef _RESULTMODE_H_
#define _RESULTMODE_H_

#include "modes.h"

class GameMode;

class ResultMode : public Mode
{
public:
    ResultMode(ModeChanger* changer, GameMode* gamemode);
    virtual ~ResultMode();

    virtual void EnterMode(double time, Mode* prev);
    virtual void LeaveMode(double time, Mode* next);

    virtual void Update(double time);
    virtual void Render(double time);

    virtual void InputAnalogAction(GameActions action, int x, int y);
    virtual void InputDigitalAction(GameActions action, int state);

private:
    GameMode* _game;
};

#endif // _RESULTMODE_H_
