#include "object.h"
#include "common/shader.h"
#include "physics.h"
#include "mesh.h"

#include <iostream>
#include <glm/gtc/type_ptr.hpp>
#include <glad/glad.h>

using namespace std;

GameObject::GameObject()
    : _phys(nullptr), _mesh(nullptr), _boundingType(GameObject::Box), _visible(true), _mass(0)
{
    this->_boundingParameters[0] = 4.0f;
    this->_boundingParameters[1] = 2.0f;
    this->_boundingParameters[2] = 4.0f;
}

GameObject::~GameObject()
{ }

void GameObject::SetPhysics(PhysicsObject* phys)
{
    this->_phys = phys;
}

void GameObject::SetMesh(PhysicsMesh* mesh)
{
    if (this->_mesh != 0)
        delete this->_mesh;
    this->_mesh = mesh;
}

PhysicsObject* GameObject::CreatePhysics(float mass)
{
    return new PhysicsObject(this, mass);
}

void GameObject::Tick(double time)
{
    this->OnTick(time);
    if (this->_phys != 0)
        this->_phys->UpdateObject(time);
}

void GameObject::Render(const glm::mat4& projection, const glm::mat4& view)
{
    glm::mat4 objmat = glm::mat4(1.0f);
    if (this->_phys != 0)
        this->_phys->_transform.getOpenGLMatrix(glm::value_ptr(objmat));

    if (this->_mesh != 0 && this->_visible)
        this->_mesh->Render(projection, view, objmat);
}
