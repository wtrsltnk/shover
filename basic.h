#ifndef BASIC_H
#define BASIC_H

#include "common/application.h"
#include "menumode.h"
#include "common/font.h"

class Basic : public Application, public ModeChanger
{
public:
    Basic();
    virtual ~Basic();

public:
    virtual void GetContextAttributes(int& major, int& minor, bool& core) { major = 4; minor = 6; core = true; }

    virtual bool InitializeApplication(System* sys);
    virtual bool InitializeGraphics();
    virtual void GameLoop();
    virtual bool IsRunning();
    virtual void Resize(int w, int h);
    virtual void InputAnalogAction(GameActions action, int x, int y);
    virtual void InputDigitalAction(GameActions action, int state);
    virtual void Close();
    virtual void Destroy();

    virtual void GotoMode(Mode* mode);
private:
    System* _sys;
    bool _running;

    MenuMode* _menu;
    Mode* _mode;
    glm::mat4 ortho;
    Font _monaco;
};

#endif // BASIC_H
