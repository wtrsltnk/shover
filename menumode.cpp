#include "menumode.h"
#include "gamemode.h"
#include "SDL.h"
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>

using namespace std;

MenuMode::MenuMode(ModeChanger* changer)
    : Mode(changer), _gamemode(nullptr)
{
    this->_monaco.Init("../shover/monaco.ttf", 24.0f);
    this->_gamemode = new GameMode(changer, this);
}

MenuMode::~MenuMode()
{ }

void MenuMode::EnterMode(double time, Mode* prev)
{
    glClearColor(0.1f, 0.5f, 0.8f, 1.0f);
}

void MenuMode::LeaveMode(double time, Mode* next)
{ }

void MenuMode::Update(double time)
{ }

void MenuMode::Render(double time)
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    int width = 800, height = 600;
    auto m = glm::ortho(0.0f, float (width), float (height), 0.0f, -10.0f, 10.0f);
    auto t2 = glm::translate(m, glm::vec3(0.0f, 48.0f, 0.0f));
    this->_monaco.PrintText(t2, "press the <space key> to start");
}

void MenuMode::InputAnalogAction(GameActions action, int x, int y)
{ }

void MenuMode::InputDigitalAction(GameActions action, int state)
{
    if (action == GameActions::NextGameMode)
        this->_changer->GotoMode(this->_gamemode);
}
