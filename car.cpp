#include "car.h"
#include "gamemode.h"
#include "mesh.h"
#include <SDL.h>
#include <iostream>

using namespace std;

Car::Car(GameMode* mode)
    : _mode(mode)
{ }

Car::~Car()
{ }

PhysicsObject* Car::CreatePhysics(float mass)
{
    return new Car::Physics(this, mass);
}

void Car::InputDigitalAction(GameActions action, int state)
{
    if (this->_phys != 0)
        this->_phys->InputDigitalAction(action, state);
}

void Car::StopCar()
{
    this->GetCarPhysics()->_speed.Reset(0);
    this->GetCarPhysics()->_direction = 0;
}

Car::Physics::Physics(GameObject* obj, float mass)
    : PhysicsObject(obj, mass), _vehicleRayCaster(0), _vehicle(0)
{
    this->_rigidBody->setActivationState(DISABLE_DEACTIVATION);

    float wheelRadius = 0.5f;
    float wheelWidth = 0.4f;
    float connectionHeight = 1.2f;
    btVector3 wheelDir(0, -1, 0);
    btVector3 wheelAxle(-1, 0, 0);
    btScalar suspensionRestLength(0.6f);

    this->_vehicleRayCaster = new btDefaultVehicleRaycaster(PhysicsManager::Instance()->_dynamicsWorld);
    this->_vehicle = new btRaycastVehicle(this->_tuning, reinterpret_cast<btRigidBody*>(this->_rigidBody), this->_vehicleRayCaster);

    PhysicsManager::Instance()->_dynamicsWorld->addVehicle(this->_vehicle);

    this->_vehicle->setCoordinateSystem(0, 1, 2);

    btVector3 pt;
    pt = btVector3(
                this->_obj->_boundingParameters[0] - (0.3f * wheelWidth),
                connectionHeight,
                this->_obj->_boundingParameters[2] * wheelRadius
            );
    this->_vehicle->addWheel(pt, wheelDir, wheelAxle, suspensionRestLength, wheelRadius, this->_tuning, true);

    pt = btVector3(
                -this->_obj->_boundingParameters[0] + (0.3f * wheelWidth),
                connectionHeight,
                this->_obj->_boundingParameters[2] * wheelRadius
            );
    this->_vehicle->addWheel(pt, wheelDir, wheelAxle, suspensionRestLength, wheelRadius, this->_tuning, true);

    pt = btVector3(
                -this->_obj->_boundingParameters[0] + (0.3f * wheelWidth),
                connectionHeight,
                -(this->_obj->_boundingParameters[2] * wheelRadius)
            );
    this->_vehicle->addWheel(pt, wheelDir, wheelAxle, suspensionRestLength, wheelRadius, this->_tuning, false);

    pt = btVector3(
                this->_obj->_boundingParameters[0] - (0.3f * wheelWidth),
                connectionHeight,
                -(this->_obj->_boundingParameters[2] * wheelRadius)
            );
    this->_vehicle->addWheel(pt, wheelDir, wheelAxle, suspensionRestLength, wheelRadius, this->_tuning, false);

    for (int i = 0; i < this->_vehicle->getNumWheels(); i++)
    {
        btWheelInfo& wheel = this->_vehicle->getWheelInfo(i);
        wheel.m_suspensionStiffness = 20.0f;
        wheel.m_wheelsDampingRelaxation = 2.3f;
        wheel.m_wheelsDampingCompression = 4.4f;
        wheel.m_frictionSlip = 1000;
        wheel.m_rollInfluence = 0.1f;
    }
}

Car::Physics::~Physics()
{ }

void Car::Physics::InputDigitalAction(GameActions action, int state)
{
    static bool brake = false;
    static int direction = 0;
    if (action == GameActions::Up && state == 1)
    {
        if (brake == false)
        {
            this->_speed.Set(100000);
            this->_direction = 1;
        }
        direction = 1;
    }
    else if (action == GameActions::Up && state == 0)
    {
        this->_speed.Set(0);
    }
    else if (action == GameActions::Down && state == 1)
    {
        if (brake == false)
        {
            this->_speed.Set(100000);
            this->_direction = -1;
        }
        direction = -1;
    }
    else if (action == GameActions::Down && state == 0)
    {
        this->_speed.Set(0);
    }
    else if (action == GameActions::Left && state == 1)
    {
        this->_vehicle->setSteeringValue(0.4f, 0);
        this->_vehicle->setSteeringValue(0.4f, 1);
    }
    else if (action == GameActions::Left && state == 0)
    {
        this->_vehicle->setSteeringValue(0, 0);
        this->_vehicle->setSteeringValue(0, 1);
    }
    else if (action == GameActions::Right && state == 1)
    {
        this->_vehicle->setSteeringValue(-0.4f, 0);
        this->_vehicle->setSteeringValue(-0.4f, 1);
    }
    else if (action == GameActions::Right && state == 0)
    {
        this->_vehicle->setSteeringValue(0, 0);
        this->_vehicle->setSteeringValue(0, 1);
    }
    //    else if (key == SDLK_SPACE && action == 1)
    //    {
    //        brake = true;
    //        this->_speed.Set(0);
    //        this->_speed.Speed(100.0f, 1000.0f);
    //    }
    //    else if (key == SDLK_SPACE && action == 0)
    //    {
    //        brake = false;
    //        if (direction != 0)
    //            this->_speed.Set(100000);
    //        this->_speed.Speed(100.0f, 100.0f);
    //    }
}

void Car::Physics::UpdateObject(double time)
{
    this->_speed.Update(time);
    this->_vehicle->applyEngineForce(this->_speed.Value() * this->_direction, 2);
    this->_vehicle->applyEngineForce(this->_speed.Value() * this->_direction, 3);
    auto children = this->_obj->_mesh->Children();
    if (children.size() == 4)
    {
        for (int i = 0; i < 4; i++)
        {
            auto wheelMesh = children[i];
            auto wheelInfo = this->_vehicle->getWheelInfo(i);
        }
    }
//    this->_vehicle->setBrake(this->_brake.Value(), 0);
//    this->_vehicle->setBrake(this->_brake.Value(), 1);
}
