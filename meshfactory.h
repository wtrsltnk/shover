/* 
 * File:   Physics.h
 * Author: Administrator
 *
 * Created on 28 oktober 2010, 2:08
 */

#ifndef MESHFACTORY_H
#define	MESHFACTORY_H

#include "mesh.h"
#include <BulletCollision/CollisionShapes/btCollisionShape.h>

class MeshFactory
{
public:
    static PhysicsMesh* CreateMesh(const btCollisionShape* shape, const glm::mat4& m = glm::mat4(1.0f));
};

#endif	/* MESHFACTORY_H */

