#include <glad/glad.h>

#include "gamemode.h"
#include "menumode.h"
#include "car.h"
#include "character.h"
#include "level.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <SDL.h>

GameMode::GameMode(ModeChanger* changer, MenuMode* menu)
    : Mode(changer), _character(nullptr), _current(nullptr), _trigger(nullptr), _menu(menu), _result(nullptr)
{
    this->_result = new ResultMode(changer, this);
}

GameMode::~GameMode()
{
    delete this->_menu;
    delete this->_result;
}

void GameMode::EnterMode(double time, Mode* prev)
{
    Level l;
    l.generate(this, this->_objects, this->_character, this->_trigger);
//    l.generateTestEnv(this, this->_objects, this->_character, this->_trigger);
    this->_current = this->_character;

    this->_updateTime = time;
    this->_renderTime = time;
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
//    PhysicsManager::Instance()->_collisionHandlers.insert(this);
    PhysicsManager::Instance()->_collisionHandlers = this;
}

void GameMode::LeaveMode(double time, Mode* next)
{
//    PhysicsManager::Instance()->_collisionHandlers.erase(this);
    PhysicsManager::Instance()->_collisionHandlers = nullptr;
}

bool GameMode::handleCollision(GameObject* a, GameObject* b)
{
    if (a == this->_trigger || b == this->_trigger)
    {
        auto other = a != this->_trigger ? a : (b != this->_trigger ? nullptr : b);
        if (other != nullptr)
        {
            if (other->type() == GameObjectTypes::SnowFlakeObject)
            {
                other->_visible = false;
                this->_objectsToDelete.push_back(other);
            }
            else
            {
                // Do something like change the game mode
            }
        }
    }
    return true;
}

void GameMode::Update(double time)
{
    this->_updateTime += time;

    for (auto i : this->_objects)
        i->Tick(time);

    PhysicsManager::Instance()->Update(float(time));

    while (!this->_objectsToDelete.empty())
    {
        auto item = this->_objectsToDelete.back();
        this->_objectsToDelete.pop_back();
        PhysicsManager::Instance()->RemoveObject(item);
        this->_objects.erase(item);
//        delete item;
    }

    btVector3 v;
    if (this->_character != nullptr)
    {
        if (this->_character->_car != nullptr)
            v = this->_character->_car->_phys->_transform.getOrigin();
        else
            v = this->_character->_phys->_transform.getOrigin();
    }
    this->_pos.x += (v.x()-this->_pos.x) / 10.0f;
    this->_pos.z += (v.z()-this->_pos.z) / 10.0f;
}

void GameMode::Render(double time)
{
    this->_renderTime += time;

    glm::mat4 view = glm::lookAt(
                glm::vec3(this->_pos.x, this->_pos.y + 120.0f, this->_pos.z),
                glm::vec3(this->_pos.x, this->_pos.y, this->_pos.z),
                glm::vec3(0.0f, 0.0f, 1.0f)
                );
    glm::mat4 projection = glm::perspective(120.0f, 800.0f/600.0f, 0.1f, 200.0f);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
//    glEnable(GL_CULL_FACE);
//    glCullFace(GL_FRONT);
    glFrontFace(GL_CCW);
//    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    for (auto i :  this->_objects)
        i->Render(projection, view);
}

void GameMode::InputAnalogAction(GameActions action, int x, int y)
{ }

void GameMode::InputDigitalAction(GameActions action, int state)
{
    if (action == GameActions::PrevGameMode && state != 0)
    {
        this->_changer->GotoMode(this->_menu);
    }
    else
    {
        if (this->_current != nullptr)
            this->_current->InputDigitalAction(action, state);
    }
}

void GameMode::GotoResultMode()
{
    this->_changer->GotoMode(this->_result);
}
