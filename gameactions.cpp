#include "gameactions.h"

std::map<InputEvent, GameActions> keyBinding;

InputEvent::InputEvent(int a, int b, int c, int d)
{
    this->data[0] = a;
    this->data[1] = b;
    this->data[2] = c;
    this->data[3] = d;
}

bool InputEvent::operator < (const InputEvent& event) const
{
    if (data[0] < event.data[0])
        return true;
    else if (data[0] == event.data[0] && data[1] < event.data[1])
        return true;
    else if (data[0] == event.data[0] && data[1] == event.data[1] && data[2] < event.data[2])
        return true;
    else if (data[0] == event.data[0] && data[1] == event.data[1] && data[2] == event.data[2] && data[3] < event.data[3])
        return true;

    return false;
}

std::ostream& operator << (std::ostream& stream, const InputEvent& event)
{
    stream << event.data[0] << ", " << event.data[1] << ", " << event.data[2] << ", " << event.data[3];
    return stream;
}
