#include "resultmode.h"
#include "gamemode.h"

ResultMode::ResultMode(ModeChanger* changer, GameMode* gamemode)
    : Mode(changer), _game(gamemode)
{ }

ResultMode::~ResultMode()
{ }

void ResultMode::EnterMode(double time, Mode* prev)
{ }

void ResultMode::LeaveMode(double time, Mode* next)
{ }

void ResultMode::Update(double time)
{ }

void ResultMode::Render(double time)
{ }

void ResultMode::InputAnalogAction(GameActions action, int x, int y)
{ }

void ResultMode::InputDigitalAction(GameActions action, int state)
{ }

