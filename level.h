#ifndef _LEVEL_H_
#define _LEVEL_H_

#include <set>
#include "object.h"
#include "character.h"
#include "gamemode.h"

class Level
{
public:
    Level();
    virtual ~Level();

    void generateTestEnv(GameMode* mode, std::set<GameObject*>& objects, Character*& character, GameObject*& trigger);
    void generate(GameMode* mode, std::set<GameObject*>& objects, Character*& character, GameObject*& trigger);
};

#endif // _LEVEL_H_
