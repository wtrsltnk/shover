#ifndef _GAMEMODE_H_
#define _GAMEMODE_H_

#include "modes.h"
#include "object.h"
#include "physics.h"
#include "car.h"
#include "character.h"
#include "trigger.h"
#include "resultmode.h"
#include "icollisionhandler.h"

#include <set>
#include <vector>

class MenuMode;

class GameMode : public Mode, public ICollisionHandler
{
public:
    GameMode(ModeChanger* changer, MenuMode* menu);
    virtual ~GameMode();

    virtual void EnterMode(double time, Mode* prev);
    virtual void LeaveMode(double time, Mode* next);

    virtual void Update(double time);
    virtual void Render(double time);

    virtual void InputAnalogAction(GameActions action, int x, int y);
    virtual void InputDigitalAction(GameActions action, int state);

    virtual bool handleCollision(GameObject* a, GameObject* b);

    void GotoResultMode();
private:
    std::set<GameObject*> _objects;
    std::vector<GameObject*> _objectsToDelete;
    glm::vec3 _pos;

    Character* _character;
    GameObject* _current;
    GameObject* _trigger;

    MenuMode* _menu;
    ResultMode* _result;
};

#endif // _GAMEMODE_H_
