#include "snow.h"
#include "physics.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

Snow::Snow(const std::vector<glm::vec3>& points)
{
    auto mass = 1.0f;
    auto shape = new btSphereShape(1.0f);

    btVector3 localInertia(0,0,0);
    shape->calculateLocalInertia(mass, localInertia);

    for (auto point : points)
    {
        auto flake = new Snow::Flake(point);
        btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, flake, shape, localInertia);
        auto rigidBody = new btRigidBody(rbInfo);

        // Lage friction betekend veel weerstand, dus neemt de snelheid snel af.
        rigidBody->setFriction(0.1f);
        rigidBody->setDamping(0.9f, 0.9f);
        rigidBody->setUserPointer(flake);

        PhysicsManager::Instance()->_dynamicsWorld->addRigidBody(rigidBody);

        this->_flakes.insert(flake);
    }
}

void Snow::Render(const glm::mat4& projection, const glm::mat4& view)
{
    this->mesh()->BindBuffers();

    glUseProgram(PhysicsMesh::shader());
    glUniformMatrix4fv(PhysicsMesh::projectionUniform(PhysicsMesh::shader()), 1, false, glm::value_ptr(projection));
    glUniformMatrix4fv(PhysicsMesh::viewUniform(PhysicsMesh::shader()), 1, false, glm::value_ptr(view));
    glUniform4fv(PhysicsMesh::colorUniform(PhysicsMesh::shader()), 1, glm::value_ptr(glm::vec4(1.0f)));
    auto modelUniform = PhysicsMesh::modelUniform(PhysicsMesh::shader());

    for (Snow::Flake* flake : this->_flakes)
    {
        if (!flake->_visible) continue;

        auto m = glm::translate(glm::mat4(1.0f), flake->position());
        glUniformMatrix4fv(modelUniform, 1, false, glm::value_ptr(m));
        this->mesh()->DrawVertices();
    }
    this->mesh()->UnbindBuffers();
}

Snow::Flake::Flake(const glm::vec3& position)
{
    this->_transform.setOrigin(btVector3(position.x, position.y, position.z));
}

void Snow::Flake::getWorldTransform(btTransform& worldTrans) const
{
    worldTrans = this->_transform;
}

void Snow::Flake::setWorldTransform(const btTransform& worldTrans)
{
    this->_transform = worldTrans;
}

glm::vec3 Snow::Flake::position() const
{
    return glm::vec3(this->_transform.getOrigin().x(), this->_transform.getOrigin().y(), this->_transform.getOrigin().z());
}

PhysicsMesh* Snow::mesh()
{
    if (Snow::_mesh == nullptr)
    {
        std::vector<Vertex> verts;
        std::vector<Mesh> meshes;

        const float PI = 3.14159f;
        float step = 0.314159f * 2.0f;
        btVector3 v = btVector3(1.0f, 1.0f, 1.0f);

        for (float angle = 0; angle <= 2 * PI; angle += step)
        {
            float x1 = (v.x()) * cos(angle);
            float z1 = (v.z()) * sin(angle);
            float x2 = (v.x()) * cos(angle+step);
            float z2 = (v.z()) * sin(angle+step);

            verts.push_back(Vertex({ glm::vec3(  x2, -v.y(),   z2) }));
            verts.push_back(Vertex({ glm::vec3(0.0f, -v.y(), 0.0f) }));
            verts.push_back(Vertex({ glm::vec3(  x1, -v.y(),   z1) }));
        }

        Mesh mesh;
        mesh._mode = GL_TRIANGLES;
        mesh._first = 0;
        mesh._count = verts.size();
        meshes.push_back(mesh);

        Snow::_mesh = new PhysicsMesh(verts, meshes);
        Snow::_mesh->setColor(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
    }

    return Snow::_mesh;
}
