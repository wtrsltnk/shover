#include "level.h"
#include "car.h"
#include "snow.h"
#include "floor.h"
#include "meshfactory.h"
#include <queue>
#include <map>
#include <random>

Level::Level()
{ }

Level::~Level()
{ }

#define WIDTH 100
#define HEIGHT 100

struct sPosition
{
    int x, y;
    bool operator < (const sPosition & other) const
    {
        if (x < other.x)
            return true;
        else if (x == other.x && y < other.y)
            return true;
        return false;
    }
    struct sPosition &SetX(float x)
    {
        this->x = x;
        return *this;
    }
    struct sPosition &SetY(float y)
    {
        this->y = y;
        return *this;
    }
};

typedef struct sPosition tPosition;

enum class ObjectTypes
{
    Put,
    CarLocation,
    PlayerLocation,
    Road,
    OffRoad,
    Tree,
    Snow
};

class Object
{
public:
    ObjectTypes type;
    tPosition position;
};

static std::map<tPosition, Object*> grid;

class AStarNode
{
public:
    AStarNode(const tPosition & pos, AStarNode * p, const tPosition & goal);

    tPosition position;
    float f, g, h;
    AStarNode *prev;

};

AStarNode::AStarNode(const tPosition & pos, AStarNode * p, const tPosition & goal) : g(10), prev(p)
{
    this->position = pos;
    if (prev != nullptr)
    {
        g += prev->g;
    }
    // manhattan distance to the goal
    this->h = (abs(goal.x - this->position.x) + abs(goal.y - this->position.y)) * 10;
    this->f = this->g + this->h;
}

class queue_item
{
public:
    queue_item(AStarNode * n);

    AStarNode *node;
    bool operator < (const queue_item & item) const;
};

queue_item::queue_item(AStarNode * n) : node(n) { }

bool queue_item::operator < (const queue_item & item) const
{
    return this->node->f < item.node->f;
}

Object *obj_GetObjectByPosition(const tPosition & pos)
{
    auto found = grid.find(pos);

    return found != grid.end() ? found->second : nullptr;
}

Object *obj_GetObjectByPosition(int x, int y)
{
    tPosition pos = { x, y };

    return obj_GetObjectByPosition(pos);
}

std::queue<tPosition> obj_GetAStarPath(const tPosition & from, const tPosition & to)
{
    std::queue<tPosition> res;

    if (from < to == to < from) return res;
    if (obj_GetObjectByPosition(to) != nullptr) return res;

    std::vector<queue_item> open;
    std::set<tPosition> closed;

    auto start = new AStarNode(from, nullptr, to);
    open.push_back(queue_item(start));

    while (!open.empty())
    {
        auto current = open.begin()->node;
        open.erase(open.begin());
        if (current->position.x == to.x && current->position.y == to.y)
        {
            std::vector<tPosition> path;
            while (current->prev != nullptr)
            {
                path.push_back(current->position);
                current = current->prev;
            }
            for (auto itr = path.rbegin(); itr != path.rend(); ++itr)
            {
                res.push(*itr);
            }
            return res;
        }
        // determine the positions of East, South, West and North
        tPosition eswn[4];
        for (int i = 0; i < 4; i++)
        {
            eswn[i].x = current->position.x;
            eswn[i].y = current->position.y;
        }
        eswn[0].x++;
        eswn[1].y++;
        eswn[2].x--;
        eswn[3].y--;
        for (int i = 0; i < 4; i++)
        {
            if (closed.find(eswn[i]) != closed.end())continue;
            if (obj_GetObjectByPosition(eswn[i]) != nullptr)continue;

            auto n = new AStarNode(eswn[i], current, to);
            auto found = open.begin();
            for (; found != open.end(); ++found)
            {
                auto p = found->node->position;
                if (p.x == eswn[i].x && p.y == eswn[i].y)
                {
                    break;
                }
            }
            if (found == open.end())
            {
                open.push_back(queue_item(n));
            }
            else
            {
                if (found->node->g > n->g)
                {
                    open.erase(found);
                    open.push_back(queue_item(n));
                }
            }
        }

        closed.insert(current->position);
    }

    return res;
}

std::queue<tPosition> obj_GetAStarPathBetweenObjects(Object * a, Object * b)
{
    return obj_GetAStarPath(a->position, b->position);
}

// simple way to find path between points
std::queue<tPosition> obj_GetSimplePath(const tPosition & from, const tPosition & to)
{
    std::queue<tPosition> res;

    if (from < to == to < from)
    {
        return res;
    }

    int x = from.x;
    int y = from.y;

    if (x != to.x)
    {
        int dirx = (from.x > to.x ? -1 : 1);
        do
        {
            x += dirx;
            tPosition p = { x, y };
            res.push(p);
        }
        while (x != to.x);
    }

    if (y != to.y)
    {
        int diry = (from.y > to.y ? -1 : 1);
        do
        {
            y += diry;
            tPosition p = { x, y };
            res.push(p);
        }
        while (y != to.y);
    }
    return res;
}

// simple way to find path between points
std::queue<tPosition> obj_GetSimplePathBetweenObjects(Object * a, Object * b)
{
    return obj_GetSimplePath(a->position, b->position);
}

std::set<Object*> obj_AllObjectsOfType(ObjectTypes type)
{
    std::set<Object*> result;

    for (auto pair : grid) if (pair.second->type == type) result.insert(pair.second);

    return result;
}

static std::default_random_engine generator;
static std::uniform_int_distribution<int> distribution(-10, 10);
static std::uniform_int_distribution<int> typecount(15, 25);
static std::uniform_int_distribution<int> oildistribution(5, 10);
static std::uniform_int_distribution<int> directiondistribution(0, 4);

Object* randomObject(ObjectTypes type)
{
    Object* found = nullptr;
    auto o = new Object();
    o->type = type;
    do
    {
        o->position.x = distribution(generator);
        o->position.y = distribution(generator);
        found = obj_GetObjectByPosition(o->position);
    }
    while (found != nullptr);

    grid.insert(std::make_pair(o->position, o));

    return o;
}

Object* createObjectAt(ObjectTypes type, int x, int y)
{
    if (obj_GetObjectByPosition(x, y) == nullptr)
    {
        auto o = new Object();
        o->type = type;
        o->position.x = x;
        o->position.y = y;
        grid.insert(std::make_pair(o->position, o));

        return o;
    }
    return nullptr;
}

void Level::generateTestEnv(GameMode* mode, std::set<GameObject*>& objects, Character*& character, GameObject*& trigger)
{
    character = new Character();
    character->_initialColor = glm::vec4(0.9f, 0.5f, 0.8f, 1.0f);
    character->_initialLocation = glm::vec3(8.0f, 0.0f, 0.0f);
    character->_boundingType = GameObject::Character;
    character->_mass = 80.0f;
    objects.insert(character);
    character->SetMesh(MeshFactory::CreateMesh(PhysicsManager::Instance()->AddObject(character, character->_mass)->_shape));
    character->_mesh->setColor(glm::vec4(0.9f, 0.2f, 0.1f, 1.0f));

    auto floor = new GameObject();
    floor->_initialLocation = glm::vec3(0.0f, -2.0f, 0.0f);
    floor->_boundingParameters[0] = 10.0f;
    floor->_boundingParameters[1] = 1.0f;
    floor->_boundingParameters[2] = 10.0f;
    floor->_mass = 0.0f;
    floor->SetMesh(MeshFactory::CreateMesh(PhysicsManager::Instance()->AddObject(floor, floor->_mass)->_shape));
    floor->_mesh->setColor(glm::vec4(0.0f, 1.0f, 1.0f, 1.0f));
    objects.insert(floor);

//    auto tree = new GameObject();
//    tree->_initialLocation = glm::vec3(0.0f, 10.0f, 1.0f);  // the origin of the cone is in its middle, not at the bottom.
//    tree->_boundingType = GameObject::Tree;
//    tree->_boundingParameters[0] = 5.0f;
//    tree->_boundingParameters[1] = 20.0f;
//    tree->_mass = 0.0f;
//    tree->SetMesh(MeshFactory::CreateMesh(PhysicsManager::Instance()->AddObject(tree, tree->_mass)->_shape));
//    tree->_mesh->setColor(glm::vec4(0.0f, 0.5f, 0.3f, 1.0f));
//    objects.insert(tree);
}

static std::uniform_int_distribution<int> treeCount(20, 50);
static std::uniform_int_distribution<int> treeHeight(40, 60);
static std::uniform_int_distribution<int> treeDiameter(4, 8);
static std::uniform_int_distribution<int> snowCount(10, 20);
static std::uniform_int_distribution<int> snowHeight(1, 100);
void Level::generate(GameMode* mode, std::set<GameObject*>& objects, Character*& character, GameObject*& trigger)
{
    const float gridCellSize = 5.0f;
    createObjectAt(ObjectTypes::Put, 0, 0);
    auto playerLoc = randomObject(ObjectTypes::PlayerLocation);
    auto carLoc = randomObject(ObjectTypes::CarLocation);

    for (int i = 0; i < treeCount(generator); i++) randomObject(ObjectTypes::Tree);
    for (int i = 0; i < snowCount(generator); i++) randomObject(ObjectTypes::Snow);

    for (int i = 0; i < typecount(generator); i++)
    {
        auto o = randomObject(ObjectTypes::OffRoad);

        for (int j = 0; j < oildistribution(generator); j++)
        {
            switch (directiondistribution(generator))
            {
            case 0: createObjectAt(ObjectTypes::OffRoad, o->position.x + 1, o->position.y); break;
            case 1: createObjectAt(ObjectTypes::OffRoad, o->position.x, o->position.y + 1); break;
            case 2: createObjectAt(ObjectTypes::OffRoad, o->position.x - 1, o->position.y); break;
            default: createObjectAt(ObjectTypes::OffRoad, o->position.x, o->position.y - 1); break;
            }
        }
    }

    tPosition min = { 9999, 9999 }, max = { -9999, -9999 };
    for (auto pair : grid)
    {
        if (pair.first.x < min.x) min.x = pair.first.x;
        if (pair.first.y < min.y) min.y = pair.first.y;
        if (pair.first.x > max.x) max.x = pair.first.x;
        if (pair.first.y > max.y) max.y = pair.first.y;
    }

    for (auto obj : obj_AllObjectsOfType(ObjectTypes::Tree))
    {
        auto height = float(treeHeight(generator));
        auto tree = new GameObject();
        tree->_initialLocation = glm::vec3(obj->position.x * gridCellSize, height / 2.0f, obj->position.y * gridCellSize);
        tree->_boundingType = GameObject::Tree;
        auto d = treeDiameter(generator);
        tree->_boundingParameters[0] = float(d);
        tree->_boundingParameters[1] = height;
        tree->_mass = 0.0f;
        tree->SetMesh(MeshFactory::CreateMesh(PhysicsManager::Instance()->AddObject(tree, tree->_mass)->_shape));
        tree->_mesh->setColor(glm::vec4(0.0f, 0.5f, 0.3f, 1.0f));
        objects.insert(tree);
    }

    std::vector<glm::vec3> points;
    for (auto obj : obj_AllObjectsOfType(ObjectTypes::Snow))
    {
        points.push_back(glm::vec3(obj->position.x * gridCellSize, snowHeight(generator) + 10.0f, obj->position.y * gridCellSize));
    }
    objects.insert(new Snow(points));

    auto walls = new GameObject[4];
    walls[0]._initialLocation = glm::vec3(201.0f, -20.0f, 0.0f);
    walls[0]._boundingParameters[0] = 2.0f;
    walls[0]._boundingParameters[1] = 20.0f;
    walls[0]._boundingParameters[2] = 200.0f;

    walls[1]._initialLocation = glm::vec3(-201.0f, -20.0f, 0.0f);
    walls[1]._boundingParameters[0] = 2.0f;
    walls[1]._boundingParameters[1] = 20.0f;
    walls[1]._boundingParameters[2] = 200.0f;

    walls[2]._initialLocation = glm::vec3(0.0f, -20.0f, -201.0f);
    walls[2]._boundingParameters[0] = 200.0f;
    walls[2]._boundingParameters[1] = 20.0f;
    walls[2]._boundingParameters[2] = 2.0f;

    walls[3]._initialLocation = glm::vec3(0.0f, -20.0f, 201.0f);
    walls[3]._boundingParameters[0] = 200.0f;
    walls[3]._boundingParameters[1] = 20.0f;
    walls[3]._boundingParameters[2] = 2.0f;
    for (int i = 0; i < 4; i ++)
    {
        walls[i]._mass = 0.0f;
        walls[i].SetMesh(MeshFactory::CreateMesh(PhysicsManager::Instance()->AddObject(&walls[i], walls[i]._mass)->_shape));
        walls[i]._mesh->setColor(glm::vec4(0.8f, 0.9f, 0.9f, 1.0f));
        objects.insert(&walls[i]);
    }

    auto car = new Car(mode);
    car->_boundingType = GameObject::CarType;
    car->_initialLocation = glm::vec3(carLoc->position.x * gridCellSize, 1.0f, carLoc->position.y * gridCellSize);
    // Wanneer deze bbox veranderd, moeten de wielen daar ook op aangepast!
    car->_boundingParameters[0] = 3.0f;
    car->_boundingParameters[1] = 2.1f;
    car->_boundingParameters[2] = 5.0f;
    car->_mass = 8000.0f;
    car->SetMesh(MeshFactory::CreateMesh(PhysicsManager::Instance()->AddObject(car, car->_mass)->_shape));
    car->GetCarPhysics()->_speed.Speed(1000.0f, 1000.0f);
    car->_mesh->setColor(glm::vec4(0.7f, 0.2f, 0.1f, 1.0f));
    objects.insert(car);

    auto  floor = new Floor(car);
    floor->_initialLocation = glm::vec3(0.0f, -1.0f, 0.0f);
    floor->_boundingParameters[0] = 200.0f;
    floor->_boundingParameters[1] = 2.0f;
    floor->_boundingParameters[2] = 200.0f;
    floor->_mass = 0.0f;
    floor->SetMesh(MeshFactory::CreateMesh(PhysicsManager::Instance()->AddObject(floor, floor->_mass)->_shape));
    floor->_mesh->setColor(glm::vec4(0.8f, 0.9f, 0.9f, 1.0f));
    objects.insert(floor);

    character = new Character();
    character->_initialColor = glm::vec4(0.9f, 0.5f, 0.8f, 1.0f);
    character->_initialLocation = glm::vec3(playerLoc->position.x * gridCellSize, 1.0f, playerLoc->position.y * gridCellSize);
    character->_boundingType = GameObject::Character;
    character->_mass = 80.0f;
    character->SetMesh(MeshFactory::CreateMesh(PhysicsManager::Instance()->AddObject(character, character->_mass)->_shape));
    character->_mesh->setColor(glm::vec4(0.9f, 0.2f, 0.1f, 1.0f));
    objects.insert(character);
}
