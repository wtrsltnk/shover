#ifndef _GAMEACTIONS_H_
#define _GAMEACTIONS_H_

#include <map>
#include <iostream>

enum class GameActions
{
    NextGameMode = 1,
    PrevGameMode = 2,
    Up = 3,
    Down = 4,
    Left = 5,
    Right = 6,
    EnterOrLeaveCar = 7
};

class InputEvent
{
public:
    InputEvent(int a, int b = 0, int c = 0, int d = 0);
    int data[4];

    bool operator < (const InputEvent& event) const;

    friend std::ostream& operator << (std::ostream& stream, const InputEvent& event);
};

std::ostream& operator << (std::ostream& stream, const InputEvent& event);

extern std::map<InputEvent, GameActions> keyBinding;

#endif // _GAMEACTIONS_H_
