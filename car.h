#ifndef _CAR_H_
#define _CAR_H_

#include "object.h"
#include "physics.h"
#include <BulletDynamics/Vehicle/btRaycastVehicle.h>

class GameMode;

class SlowValue
{
public:
    SlowValue()
        : _target(0), _actual(0), _speedUp(1.0f), _speedDown(1.0f)
    { }

    void Update(double time)
    {
        if (this->_actual != this->_target)
        {
            if (this->_actual > this->_target)
            {
                this->_actual -= this->_speedDown;
                if (this->_actual < this->_target)
                    this->_actual = this->_target;
            }
            else if (this->_actual < this->_target)
            {
                this->_actual += this->_speedUp;
                if (this->_actual > this->_target)
                    this->_actual = this->_target;
            }
        }
    }

    float Value() { return this->_actual; }
    void Set(float target) { this->_target = target; }
    void Reset(float actual) { this->_actual = this->_target = actual; }
    void Speed(float up, float down) { this->_speedUp = up; this->_speedDown = down; }

private:
    float _target;
    float _actual;
    float _speedUp;
    float _speedDown;

};

class Car : public GameObject
{
public:
    class Physics : public PhysicsObject
    {
    public:
        Physics(GameObject* obj, float mass);
        virtual ~Physics();

        virtual const char* Id() { return "Car"; }
        virtual void UpdateObject(double time);
        virtual void InputDigitalAction(GameActions action, int state);

        SlowValue _speed;
        int _direction;
        btDefaultVehicleRaycaster* _vehicleRayCaster;
        btRaycastVehicle* _vehicle;
        btRaycastVehicle::btVehicleTuning _tuning;

    };

public:
    Car(GameMode* mode);
    virtual ~Car();

    virtual PhysicsObject* CreatePhysics(float mass);
    virtual void InputDigitalAction(GameActions action, int state);

    virtual GameObjectTypes type() { return GameObjectTypes::CarObject; }

    Car::Physics* GetCarPhysics() { return (Car::Physics*)this->_phys; }
    void StopCar();

protected:
    PhysicsMesh* _wheelMeshes[4];
    GameMode* _mode;
};

#endif // _CAR_H_
