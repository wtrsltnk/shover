#ifndef _OBJECT_H_
#define _OBJECT_H_

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <string>
#include <vector>
#include "gameactions.h"

class PhysicsMesh;
class PhysicsObject;

enum class GameObjectTypes
{
    GameObject = 0,
    CarObject = 1,
    CharacterObject = 2,
    TreeObject = 3,
    SnowObject = 4,
    SnowFlakeObject = 5,
    FloorObject = 6
};

class GameObject
{
public:
    enum BoundingType
    {
        Sphere,     // With radius
        Box,        // Box with variable width, height, depth
        Cylinder,   // With variable width, height, depth
        Tree,
        Character,  // Capsule with radius, height
        CarType
    };

public:
    GameObject();
    virtual ~GameObject();

    virtual PhysicsObject* CreatePhysics(float mass);

    virtual void Tick(double time);
    virtual void Render(const glm::mat4& projection, const glm::mat4& view);

    void SetPhysics(PhysicsObject* phys);
    void SetMesh(PhysicsMesh* mesh);

    virtual void OnTick(double time) { }
    virtual void InputDigitalAction(GameActions key, int state) { }

    virtual GameObjectTypes type() { return GameObjectTypes::GameObject; }

    glm::vec3 _initialLocation;
    glm::quat _initialOrientation;
    glm::vec4 _initialColor;
    PhysicsObject* _phys;
    PhysicsMesh* _mesh;
    BoundingType _boundingType;
    float _boundingParameters[6];
    bool _visible;
    float _mass;
};

#endif // _OBJECT_H_
